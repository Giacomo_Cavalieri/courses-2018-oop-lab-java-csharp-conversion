package it.unibo.oop.mnk;

import it.unibo.oop.events.EventSource;
import it.unibo.oop.util.ImmutableMatrix;

public interface MNKMatch {

    ImmutableMatrix<Symbols> getGrid();

    int getK();

    void move(int i, int j);

    void reset();

    int getTurn();

    Symbols getCurrentPlayer();

    EventSource<TurnEventArgs> turnEnded();
    EventSource<TurnEventArgs> turnBeginning();
    EventSource<Exception> errorOccurred();
    EventSource<MatchEventArgs> matchEnded();
    EventSource<MNKMatch> resetPerformed();


    static MNKMatch of(int m, int n, int k) {
        return new MNKMatchImpl(m, n, k);
    }

    static MNKMatch of(int m, int n, int k, Symbols first) {
        return new MNKMatchImpl(m, n, k, first);
    }
}
