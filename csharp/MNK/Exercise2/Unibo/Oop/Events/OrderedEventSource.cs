﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unibo.Oop.Events
{
    class OrderedEventSource<TArg> : AbstractEventSource<TArg>
    {
        private IList<EventListener<TArg>> listeners = new List<EventListener<TArg>>();

        protected override ICollection<EventListener<TArg>> EventListeners => listeners;
    }
}
