﻿using System;
using System.Collections.Generic;

namespace Unibo.Oop.Events
{
    abstract class AbstractEventSource<TArg> : IEventEmitter<TArg>, IEventSource<TArg>
    {
        protected abstract ICollection<EventListener<TArg>> EventListeners {
            get; 
        }

        public IEventSource<TArg> EventSource => this;

        public void Bind(EventListener<TArg> eventListener)
        {
            EventListeners.Add(eventListener);
        }

        public void Emit(TArg data)
        {
            foreach(EventListener<TArg> listener in EventListeners)
            {
                listener(data);
            }
        }

        public void Unbind(EventListener<TArg> eventListener)
        {
            EventListeners.Remove(eventListener);
        }

        public void UnbindAll()
        {
            EventListeners.Clear();
        }
    }
}
